delimiter //
create function getMinExperienceFromEmployee() returns decimal(10,1)
reads sql data
deterministic
begin
	declare min_exp decimal(10, 1);
 	select MIN(experience) from employee into min_exp;
	return min_exp;
end //
delimiter ;

#===================================================================================================

delimiter //
create function getFullPharmacyAdress(p_id int) returns varchar(500)
reads sql data
deterministic
begin
	declare fullAdress varchar(50);
	declare p_name varchar(50);
	declare b_numb int;
	select  name, building_number into p_name, b_numb from pharmacy where id = p_id;
	set fullAdress = concat(p_name, ' ', b_numb);
	return fullAdress;
end //
delimiter ;